// The Template Loader. Used to asynchronously load templates located in separate .html files
window.templateLoader = {

    load: function(views, callback) {

        var deferreds = [];

        $.each(views, function(index, view) {
            if (window[view]) {
                deferreds.push($.get('tpl/' + view + '.html', function(data) {
                    window[view].prototype.template = _.template(data);
                }, 'html'));
            } else {
                alert(view + " not found");
            }
        });

        $.when.apply(null, deferreds).done(callback);
    }

};

window.validationUtils = {

    isUniqueUser: function(email, success, error) {
        $.ajax({
			type: "GET",
			url: "http://localhost/heylikeylikey/api/index.php/isuniqueuser/"+email+"?username="+Cookie.get('username')+"&sessionid="+Cookie.get('sessionid'),
			success: success,
			error: error,			
			dataType: "json"
		});
    }

};


String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.truncate = function(n){
    return this.substr(0,n-1)+(this.length>n?'&hellip;':'');
};

Parse.initialize("NekLBV7aa53USWhSwymv1LGo29ORG6fPgKEikWCi", "FZj1JD8rr9Rvk7w6mgpLlC3bUM6z7IMavdznfuVy");