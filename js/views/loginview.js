var LoginView = FormView.extend({
	
	events: {
		"click .send"	: "saveForm"
    },
    
    initialize: function () {
    	$('#fail').hide();
    }, 

	render: function() {
		$(this.el).html(this.template());
		return this;
	},

	saveForm: function() {
		$('#fail').hide();
		Parse.User.logIn($('#username').val(), $('#password').val(), {
		  success: function(user) {
		    window.location = 'index.html';
		  },
		  error: function(user, error) {
		    $('#fail').show();
		  }
		});
		return false;
	}
});