var CalenderView = Backbone.View.extend({

    initialize:function () {

    },

    render:function (eventName) {
        $(this.el).html(this.template());
        var div = this.$('#calender');
        div.clndr({
          startWithMonth: moment(),
          clickEvents: {
            click: function(target){
              
              var date = new Date(target.date._a[0], target.date._a[1], target.date._a[2] )

              $("#takingsModal").empty();
              var model = new Takings({'date': date});
              var view = new NewTakingsView({model:model});
              $('#takingsModal').append(view.render().el);
              $('#takingsModal').foundation('reveal', 'open');
            },

          }
        });
        return this;
    },

    close:function () {

    }
});
