window.Header = Backbone.View.extend({

    initialize:function () {},

    events: {
        "click .likeylikey" : "likey"
    },

    likey: function() {
        this.create();
    },

    create: function(){
        $("#takingsModal").empty();

        var date = new Date();
        var model = new Takings({'date':date});
        var view = new NewTakingsView({model:model});
        $('#takingsModal').append(view.render().el);
        $('#takingsModal').foundation('reveal', 'open');
    },

    render:function () {
        $(this.el).html(this.template());
        return this;
    }

});
