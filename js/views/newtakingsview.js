var NewTakingsView = FormView.extend({

	events: {
		"click .save" : "saveForm",
    },


	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		$('#day').val(this.model.get('date'));
		return this;
	},

	saveForm: function() {
		var takings = this.model;
		takings.set('total', $('#total').val());

		//takings.setACL(new Parse.ACL(Parse.User.current()));


		takings.save(null,{
			success: function (takings) {
				$('#takingsModal').foundation('reveal', 'close');
				app.navigate("",true);
			},
			error: function(takings, error) {
			    // Execute any logic that should take place if the save fails.
			    // error is a Parse.Error with an error code and description.
			    alert('Failed to create new object, with error code: ' + error.message);
			  }
		});
		return false;
	}
});
